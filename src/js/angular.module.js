(function() {
    'use strict';
    
    angular.module('filterTreeApp', [])
        .controller('filterTreeController', filterTreeController )
        
        
            
    
    filterTreeController.$inject = ['$scope'];
    
    function filterTreeController($scope) {
        
        
        $scope.fieldList = [{
                artifactID: 1,
                displayName: 'Doc ID',
                fieldTypeID: 0,
                codeTypeID: null,
                fieldArtifactTypeID: 10
            }, {
                artifactID: 2,
                displayName: 'Artifact ID',
                fieldTypeID: 1,
                codeTypeID: null,
                fieldArtifactTypeID: 10
            }, {
                artifactID: 3,
                displayName: 'Date Modified',
                fieldTypeID: 2,
                codeTypeID: null,
                fieldArtifactTypeID: 10
            }, {
                artifactID: 4,
                displayName: 'Has Native',
                fieldTypeID: 3,
                codeTypeID: null,
                fieldArtifactTypeID: 10
            }, {
                artifactID: 5,
                displayName: 'Pet Types',
                fieldTypeID: 5,
                codeTypeID: 1000100,
                fieldArtifactTypeID: 10
            }, {
                artifactID: 6,
                displayName: 'Decimal Field',
                fieldTypeID: 6,
                codeTypeID: null,
                fieldArtifactTypeID: 10
            }, {
                artifactID: 7,
                displayName: 'Currency Field',
                fieldTypeID: 7,
                codeTypeID: null,
                fieldArtifactTypeID: 10
            }, {
                artifactID: 8,
                displayName: 'Container Types',
                fieldTypeID: 8,
                codeTypeID: 1000120,
                fieldArtifactTypeID: 10
            }];
        $scope.test = 'test';
        
        $scope.chartFilters = {};
        
        $scope.popSearchSummary = popSearchSummary;
        
        function popSearchSummary () {
            
        }

    }
    

})()