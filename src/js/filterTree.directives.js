(function() {
    'use strict';
    
    angular
        .module('filterTreeApp')
        .directive('filterTree', filterTree)
        .directive('filterTreeGroupHeader', filterTreeGroupHeader)
        .directive('filterTreeFilterItem', filterTreeFilterItem)


    
    
    
    function filterTree() {
        return {
                restrict: 'E',
                replace: true,
                templateUrl: 'views/filterTree.html',
                controller: ftreeCtrl,
                controllerAs: '$ftree',
                scope: {
                    fieldList: '=',
                    operatorList: '=',
                    filterInfo: '='
                }

         }
    }
    
    ftreeCtrl.$inject = ['$scope', '$sce']
    
    function ftreeCtrl ($scope, $sce) {
         var ctrl = this;

        // from scope
        ctrl.itemCounter = 1;
        ctrl.test = $scope.test;
        ctrl.fieldList = $scope.fieldList;
        ctrl.operatorList = $scope.operatorList;

        
        ctrl.searchSummary = '<ul><li class="ftss">Empty</li></ul>';

        // templates        
        ctrl.data = {
                id: 1,
                class: "gh",
                operator: "AND",
                children: []
            };
        ctrl.dateSelectorOptions = ['Selected Date', 'Now', 'Yesterday', 'Today', 'Tomorrow', 'This Calendar Week', 'This Calendar Month', 'This Calendar Year'];

        ctrl.ghTemplate = {
                id: -1,
                class: "gh",
                operator: "AND",
                children: []
            };

        ctrl.fiTemplate = {
            id: -1,
            class: "fi",
            dateSelector: 'Selected Date',
            field: {},
            fieldType: 'text',
            operator: 'is',
            operatorList: ['is'],
            value: ''
        }
        ctrl.searchSummaryObjectOriginal = angular.copy(ctrl.data);

        // functions
        ctrl.addFilterItem = addFilterItem;
        ctrl.addGroupHeader = addGroupHeader;
        ctrl.closeAll = closeAll;
        ctrl.close = close;
        ctrl.getChildClass = getChildClass;
        ctrl.changeJoinOperator = changeJoinOperator;           
        ctrl.fieldSelected = fieldSelected;
        ctrl.moveUp = moveUp;
        ctrl.operatorSelected = operatorSelected;
        ctrl.renderHtml = renderHtml;
        ctrl.setDateValue = setDateValue;
        ctrl.updateSearchSummary = updateSearchSummary; 


        // function constructors
        function addFilterItem(node) {
            // add the object to the searchSummaryObject
            ctrl.itemCounter++;
            var newFI = angular.copy(ctrl.fiTemplate);
            newFI.id = ctrl.itemCounter;
            node.children.push(newFI);
            updateSearchSummary();
        }

        function addGroupHeader(node) {
            ctrl.itemCounter++;
            
            if(node.id === 1 && node.children.length == 0 ){
                // if this is the first time hitting the split icon, just add a filter item, instead of an additional group header
                addFilterItem(node);
                return
            }
            
            var newGH = angular.copy(ctrl.ghTemplate);
            newGH.id = ctrl.itemCounter;
            // check the type of the node that is adding this request.
            // if it's a groupheader, add the new group header as a child.
            // if it's a filter item, we'll need to figure out how to add the group child as a parent to the current element

            if (node.class === 'gh') {
                // if this is a group header, add a filter item to the new group header and then add the group header.
                addFilterItem(newGH);
                node.children.push(newGH);
            } else if (node.class === 'fi') {
                // need to find the parent.  can do this by ID
                var parentID = findParentID(ctrl.data, node);
                
                //now, a function to get the object of the parent.
                var parentObject = findObjectByID(ctrl.data, parentID);
                var currentIndex = parentObject.children.indexOf(node);
                                
                // add current node to new gh.
                newGH.children.push(node);
                
                // add new filter item to new gh (this way there is an additional filter item to group by)
                addFilterItem(newGH);
                
                //remove current node from parent and replace with new GH
                parentObject.children.splice(currentIndex, 1, newGH);
                


            }
                updateSearchSummary();
        }
        
           
        function changeJoinOperator(node) {
            if (node.operator === 'AND') {
                node.operator = 'OR';
            } else {
                node.operator = 'AND';
            }
            updateSearchSummary();
        }

        function closeAll() {
            ctrl.data = angular.copy(ctrl.searchSummaryObjectOriginal);
            updateSearchSummary();
        }

        function close(fullObject, subObject) {
            // still needs work.  need to find how to reach the parent of the current node.
            var results;
            
            if (fullObject === subObject && subObject.id === 1) {
              ctrl.data = angular.copy(ctrl.searchSummaryObjectOriginal);
              results = true;
              return results;
            }
            for(var i=0; i < fullObject.children.length; i++){
              if (fullObject.children[i].id == subObject.id) {
                fullObject.children.splice(i, 1);
                  results = true;
                break;
              } else if (fullObject.children[i].hasOwnProperty('children') && results === undefined) {
                 results = close(fullObject.children[i], subObject);
              }
            }
            updateSearchSummary();
            return results;           
        }

        function fieldSelected (node) {
            node.value = '';
            // set node specific operator list
            node.operatorList = setOperatorList(node.field.fieldTypeID);
            // set operator as first operator
            node.operator = node.operatorList[0];
            // set fieldType = text or date
            node.fieldType = setFieldType(node.field.fieldTypeID);
            updateSearchSummary();
        }
        

        function getChildClass(child) {
            if (child.class === 'fi') {
                return 'views/ftreeFilterItem.html';
            } else if (child.class === 'gh') {
                return 'views/ftreeGroupHeader.html';
            } else {
                return '';
            }
        }
        
        function moveUp(node) {
            //find parentID
            var parentID = findParentID(ctrl.data, node);
            var parentObject = findObjectByID(ctrl.data, parentID);
            var currentIndex = parentObject.children.indexOf(node);
            // if parentID = 1, need to remove this GH and make all children part of the top level node.
            // extract any children from the current node
            if (node.children.length > 0 ){
                // push them to the parentID in the same position as their parent (deleting the parent in the process)
                Array.prototype.splice.apply(parentObject.children, [currentIndex, 1].concat(node.children));
            } else { //the node is empty, so just remove it.
                close(ctrl.data, node);
            }
        }
        
        function operatorSelected(object){
            if (object.operator === 'is set' || object.operator === 'is not set'){
                object.value = '';
            }
            updateSearchSummary();
        }
        
        function renderHtml(html_code) {
            return $sce.trustAsHtml(html_code);
        }
        
        function setDateValue(object) {
            // use switch to set object.value to various things
            /* selected date
            now
            yesterday
            today
            tomorrow
            this calendar week, month, and year
            */
            var today, todayNumber, mondayNumber, sundayNumber, monday, sunday;

            
            var d;
            switch (object.dateSelector) {
                case 'Selected Date':
                    break;
                /*
                case 'Now': 
                    object.value = Date.now();
                    break;
                case 'Yesterday': 
                    d = new Date();
                    d.setDate(d.getDate() - 1);
                    object.value = d;
                    break;
                case 'Today': 
                    object.value = new Date();
                    break;
                case 'Tomorrow': 
                    d = new Date();
                    d.setDate(d.getDate() + 1);
                    object.value = d;
                    break; 
                */
                default:
                    object.value = '';
             }
            updateSearchSummary();
        }
        
        function updateSearchSummary() {
            if (ctrl.data.children.length == 0) {
                ctrl.searchSummary = '<ul><li class="ftss">Empty</li></ul>';
                return
            } else {
                ctrl.searchSummary = generateFriendlySearch(ctrl.data);
            
            }
            $scope.filterInfo = ctrl.data
        }
        
        
        //// non-scope functions
        
        function findObjectByID(object, oid){
            var results;
            
            if(object.id === oid){
                results = object;
                return results;
            } else {
                  for(var i = 0; i<object.children.length; i++) {
                    if(object.children[i].id === oid) {
                        results = object.children[i];
                        return results;
                        break;
                    } else if (object.children[i].hasOwnProperty('children') && results === undefined) {
                        results = findObjectByID(object.children[i], oid);
                    }
                }

            }
             return results;
        }

        function findParentID(fullObject, subObject) {
            var results; 
            
            if (fullObject === subObject && subObject.id === 1) {
              results = 1;
                return results;
            } else {
                for(var i=0; i < fullObject.children.length; i++){
                  if (fullObject.children[i].id == subObject.id) {
                      results = fullObject.id;
                      return results;
                      break;
                  } else if (fullObject.children[i].hasOwnProperty('children') && results === undefined) {
                      results = findParentID(fullObject.children[i], subObject);
                  }
                }
            }
            return results;
        }

        function generateFriendlySearch(object) {
            var searchSummary = '<ul>';
            
            var ghOperator = object.operator;
            
            // if the object only has one child and it's a filter item:
            if (object.children.length == 1 && object.children[0].class === 'fi') {
                searchSummary = searchSummary + setFISS(object.children[0]);        
            } else {
                for(var i = 0; i<object.children.length; i++) {
                    if (object.children[i].class === 'fi'){
                        searchSummary = searchSummary + setFISS(object.children[i]);
                    } else {
                        searchSummary = searchSummary + setGHSS(object.children[i]);
                    }
                    if (i < object.children.length -1 ){
                        searchSummary = searchSummary + '<li class="ftss">' + ghOperator + '</li>';
                    }
                }
                
            }
            searchSummary = searchSummary + '</ul>';
            // if we didn't actually add anything to the search summary, reset it back to empty
            if (searchSummary === '<ul></ul>') {
                searchSummary = '<ul><li class="ftss">Empty</li></ul>';
                return searchSummary;
            }
            return searchSummary;
        }
        
        function setFISS (object){
            var searchSummary = '';
            var fieldName, fiOperator, value;
            
            fieldName = object.field.displayName || 'null';
            fiOperator = object.operator || 'is';
            value = setValueDisplay(object);
            searchSummary = '<li class="ftss">' + fieldName + ' ' + fiOperator + ' ' + value + '</li>';

            return searchSummary;
        }
        
        function setGHSS(object){
            var searchSummary = '';
            var ghOperator = object.operator;
            if (object.children.length > 0){
                searchSummary = '<li class="ftss">(<ul>';
                for(var i=0; i<object.children.length; i++) {
                    if (object.children[i].class === 'fi') {
                        searchSummary = searchSummary + setFISS(object.children[i]);
                    } else {
                        searchSummary = searchSummary + setGHSS(object.children[i]);
                    }
                    if (i < object.children.length -1 ){
                        searchSummary = searchSummary + '<li class="ftss">' + ghOperator + '</li>';
                    }
                }
                searchSummary = searchSummary + '</ul></li><li class="ftss">)</li>';
            }
            return searchSummary;
        }       
        
        function setOperatorList(fieldTypeID) {
            var operators = [];

            if (fieldTypeID === undefined) {
                operators.push('is');
            } else {
                switch (fieldTypeID) {
                    case 0: // fixed length text
                        operators.push('is', 'is not', 'is set', 'is not set', 'is less than', 'is greater than', 'is like', 'is not like');
                        break;
                    case 2: // date
                        operators.push('is', 'is not', 'is set', 'is not set', 'is beofre', 'is before or on', 'is after', 'is after or on', 'between', 'is in');
                        break;
                    case 3: // yes/no
                        operators.push('is', 'is not', 'is set', 'is not set');
                        break;
                    case 1: // whole number
                    case 6: // decimal
                    case 7: // currency
                        operators.push('is', 'is not', 'is set', 'is not set', 'is less than', 'is greater than');
                        break;
                    case 5: //single choice
                        operators.push('any of these', 'none of these', 'is set', 'is not set');
                        break;
                    case 8: // multiple choice
                        operators.push('any of these', 'none of these', 'all of these', 'not all of these', 'is set', 'is not set');
                        break;
                }
            }

            return operators;
        }
        
        function setFieldType(fieldTypeID) {
            // just basic for now.  if fieldTypeID === 2, we'll retur "date"
            // later, we can add another option for choices (fieldTypeID === 5 or 8)
            var type = 'text';
            if (fieldTypeID === 2) {
                type = 'date';
            }
            
            return type;
        }
    
        function setValueDisplay(object) {
            var results;
            if (object.operator === 'is set' || object.operator === 'is not set'){
				results = '';
				return results;
			} else {
			
				if (object.fieldType === 'text') {
					results = object.value || '&lt;Unset&gt;';
				} else if (object.dateSelector === 'Selected Date'){
					if (object.value === ''){
						results = '&lt;Unset&gt;';
					} else {
						results = object.value.getFullYear() + '-' + ('0' + (object.value.getMonth() + 1)).slice(-2) + '-' + ('0' + object.value.getDate()).slice(-2);   
					}
					
				} else {
					results = object.dateSelector;
				}
			}
            return results;
        }
        
    }
        
    
    function filterTreeGroupHeader() {
        return {
            restrict: 'E',
            require: '^filterTree',
            templateUrl: 'views/ftreeGroupHeader.html',
            scope: true
        }
    }
    
    function filterTreeFilterItem() {
        return {
            restrict: 'E',
            require: '^filterTree',
            templateUrl: 'views/ftreeFilterItem.html',
            scope: true
        }
    }
    
        
})();