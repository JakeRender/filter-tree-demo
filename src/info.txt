            
    
    /*
        goal: create a tree view that allows users to create where clauses for sql searches.
            will have a ui that lets users craft custom filters
            will also display current filter string in a search summary box below
            
        UI: will start with a groupHeader div and a searchSummary box.
                div will contain close button, up arrow, and/or, add filter, create grouping
            Clicking Add filter creates a UL with a single LI in it.
                This LI will have a close button, select field, select operator (populated based on field), a value field, and a create grouping button
                As it is populated, the search summary box will also be populated
            Clicking create grouping will insert a new join div as a new UL with a new filter
            
            
        Terms:
            groupHeader: this is the UL and LI that contains the close button, up arrow, and/or, add filter, create grouping buttons
            filterItem: this is the LI that contains the close button, field selector, operator selector, value box, and create grouping button
            
            closeButton: will remove attached filterItem, or groupHeader
            upArrow: Only visible on groupHeaders.  
                If groupHeader is a child of another group, moves it up one level.  
                If moved to topmost group, groupHeader is removed and all children are put into the topmost groupHeader (keeping child relationships intact)
            andOr: Only visible on groupHeaders.  controls and/or status of the current group
            createGroup: will create a groupHeader and filterItem as child of current group.
                when clicked on current groupHeader, this adds another groupHeader below it.
                
        
            
    */



directives and variables:
    filterTree: $ftree
        fieldList array
        operatorList array
        searchSummary string
        searchSummaryObject object
        populateSearchSummary(ssObject) function

    ftreeGroupHeader: $ftreeGroupHeader
        close() function
        changeJoinOperator() function
        joinOperator string
        addFilterItem() function
        addGroupHeader() function

    ftreeFilterItem: $ftreeItem
        close() function
        filterField object
        fieldSelected() function
        fieldList array
        operator string
        operatorList array
        operatorSelected() function
        searchSummaryObject object
        disableInput bool
        showDatePicker bool
        showInputText bool
        addGroupHeader() function

    ftreeFilterItemValue: $ftreeItemValue
        value string
        disableInput bool
        updateSearchSummary()
        showDatePicker bool
        showInputText bool

styles:
    filter-list
    filter-search-summary
    filter-tree
    filter-tree-group-header
    jr-circle
    jr-circle-hover
    jr-fa-green
    jr-fa-red
    filter-tree-value
    filter-tree-filter-item
